
#### Steps to run the project:

_Run in terminal_

1. `git clone https://bitbucket.org/maryf82/reactcountdownclock.git` to copy project to local folder __reactcountdownclock__

2. `cd reactcountdownclock/` go into reactcountdownclock folder

3. `npm install` install required libraries (dependencies)

4. `npm start` start server. Project will start in browser automatically
