import React from 'react';
import './App.css';


let interval

class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      date: null,
      now: null
    }
  }

  dateChangeHandler(e) {

    this.setState({ date: e.target.value })

    this.count()
  }

  count() {
    if (interval) {
      clearInterval(interval)
    }

    interval = setInterval(() => {
      this.forceUpdate()
    }, 1000);
  }

  render() {
    let days, hours, minutes, seconds

    if (this.state.date) {
      let timestamp = Date.parse(this.state.date)
      let delta = timestamp - Date.now()
      seconds = delta / 1000;
      minutes = seconds / 60;
      hours = minutes / 60;
      days = hours / 24;
    }


    return (
      <div className="App">
        <input
          type='date'
          onChange={this.dateChangeHandler.bind(this)}
        />
        <h2>
          {this.state.date ?
            `Countdown to
            ${this.state.date.split('-')[1]}/
            ${this.state.date.split('-')[2]}/
            ${this.state.date.split('-')[0]}
            `
            :
            `Select Date to countdown.`
          }
        </h2>

        {this.state.date ?
          <div className="clock">
            <div className="clockDays">{Math.floor(days)} day(s)</div>
            <div className="clockHours">{Math.floor(hours % 24)} hour(s)</div>
            <div className="clockMinutes">{Math.floor(minutes % 60)} minute(s)</div>
            <div className="clockSeconds">{Math.floor(seconds % 60)} second(s)</div>
          </div>
          : null}

        <div>

        </div>


      </div>
    )
  }

}

export default App;
